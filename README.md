# SPooN ROS messages and services

## Getting started

### Setting up
We use [ROS Noetic](http://wiki.ros.org/noetic/Installation/Ubuntu) on a machine running Ubuntu to run the rosbridge. 

Once you've done with the install of ROS Noetic (the `ros-noetic-desktop-full` meta package), you'll need to install additional ROS packages:

```
sudo apt install ros-noetic-rosbridge-suite ros-noetic-audio-common
```

SPooN connects to the rosbridge by adding the following lines to the release.conf on the windows machine - **replace** `192.168.0.2` by the ip address of the machine running rosbridge (and make sure it's accessible by the machine running SPooN's software).

```
"ROS": 
  {
    "ROSEnabled": true,
    "ROSBridgeURL": "ws://192.168.0.2",
    "ROSBridgePort": 9090
  }
```

You'll need to [create a catkin workspace](http://wiki.ros.org/catkin/Tutorials/create_a_workspace) for this ROS package in order to have access to SPooN messages from ROS:
1. `source /opt/ros/noetic/setup.bash`
2. `mkdir -p ~/spoon_ws/src`
3. `cd ~/spoon_ws/src/`
4. `git clone https://gitlab.com/SpoonQIR/Innovation/ros/spoon-msg.git`
5. `catkin_make`
6. `source devel/setup.bash`

If you want to setup this workspace as your default workspace for your user, you can add the following line at the end of your `~/.bashrc`:

```
source ~/spoon_ws/src/devel/setup.bash
```

### Running rosbridge
Start a rosbridge (on the Linux machine) before starting SPooN's software (on the windows machine): `roslaunch rosbridge_server rosbridge_websocket.launch`.

Then start SPooN's software. 

You should see the following logs in the rosbridge console:
```
...
2023-02-20 15:43:43-0500 [-] [INFO] [1676925823.942053]: Rosbridge WebSocket server started at ws://0.0.0.0:9090
2023-02-20 15:56:21-0500 [-] [INFO] [1676926581.875515]: Client connected.  1 clients total
```


## Rosbag

### Recording a rosbag
Run `rosbag record -a` to start recording everything.

Stop the recording by pressing `ctrl+c`.

### Looking at the data
You can use the `rqt_bag` program to look at the data in your rosbag: `rqt_bag name_of_the_bagfile.bag`.
